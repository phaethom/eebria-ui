
import 'antd/dist/antd.css';
import './index.css';
import { Layout, Menu, Breadcrumb, Select } from 'antd';
import { UserOutlined} from '@ant-design/icons';
import Clock from 'react-live-clock';

import { Product } from '../pages/Product'


import { Container, LogoContainer, LogoImage, EbriaTradeTitle, EbriaTradeTitleContainer } from './style';

import logo from '../../components/img/logo.png';

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout

const { Option } = Select;
function handleChange(value) {
  console.log(value);
}

function Layouts() {

  const dt = new Date();
  const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


  return (
    <Layout>
      <Header className="header">
        <LogoContainer>
          <LogoImage src={logo} alt="Ebria Logo" />
        </LogoContainer>
      </Header>
      <Content style={{ padding: '0 50px' }}>

        <Breadcrumb style={{ margin: '16px 0' }}>

          <EbriaTradeTitleContainer>
            <EbriaTradeTitle>EebriaTrade - Products</EbriaTradeTitle>
          </EbriaTradeTitleContainer>

        </Breadcrumb>
        <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{ height: '100%' }}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="John">
                <Menu.Item key="1">Settings</Menu.Item>
                <Menu.Item key="2">Logout</Menu.Item>
              </SubMenu>

            </Menu>
          </Sider>
          <Content style={{ padding: '0 24px', minHeight: 280 }}>
            <Product >
              
                </Product>
          </Content>
        </Layout>
      </Content>
      <Footer style={{ textAlign: 'center' }}> ©2021 Created by Odair Pires {dt.getDay()} {months[dt.getMonth()]} {dt.getUTCFullYear()} <Clock format={'HH:mm:ss'} ticking={true} timezone={'EUROPE/London'} /></Footer>
    </Layout>
  )
}

export { Layouts };
