
import ProductCard from '../productCard/ProductCard'
import { Container } from './style';

const ProductList = (props) => {
  const products = props.products.map((product) => {
    return <ProductCard product={product} />
  });
  return <Container>{products}</Container>;
}
export default ProductList;