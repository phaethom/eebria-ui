import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Select, Divider, Checkbox, Input, Search } from 'antd';
import ProductList from '../productList/ProductList';
import { Container, ListOfItemsContainer, OptionContainer, SearchContainer } from './style';


const { Option } = Select;

const CheckboxGroup = Checkbox.Group;

const plainOptions = ['Cheaper', 'Expensive'];
const defaultCheckedList = ['Cheaper', 'Expensive'];
function Product() {
  const { Search } = Input;
  const [products, setProducts] = useState([]);

  const [products2, setProducts2] = useState([]);

  const onSearch = value => {
    const bev = products.filter(pilot => pilot.name === value);
    setProducts(bev);
    console.log(bev)
    console.log(value)

  };
  const getAllProducts = async () => {
    const response = await axios.get('http://api.eebria.com/',);

    setProducts(response.data);

  }

  function handleChange(value) {

    if (value.trim() === "beer") {
      const beers = products.filter(beer => beer.style === value);
      setProducts(beers);
    } else if (value.trim() === "cider") {
      const ciders = products.filter(cider => cider.style === "cider");
      setProducts(ciders);
    } else {
      getAllProducts();
    }

  }

  useEffect(async () => {
    getAllProducts();

  }, [])
  const [checkedList, setCheckedList] = React.useState(defaultCheckedList);
  const [indeterminate, setIndeterminate] = React.useState(true);

  const onChange = list => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < plainOptions.length);
  };
  const onCheckAllChange = e => {
    setCheckedList(e.target.checked ? plainOptions : []);
    setIndeterminate(false);
  };
  return (
    <Container>
      <OptionContainer>
        <SearchContainer>
          <Search placeholder="Search by Name" allowClear onSearch={onSearch} style={{ width: 400 }} />
        </SearchContainer>
        <Select defaultValue="Style" Style={{ with: 200 }} onChange={handleChange}>
          <Option value="beer">Beer</Option>
          <Option value="cider">Cider</Option>
        </Select>

        <CheckboxGroup options={plainOptions} value={checkedList} onChange={onChange} />

      </OptionContainer>

      <Divider />
      <ListOfItemsContainer>
        <ProductList products={products} />
        {console.log(products2)}
      </ListOfItemsContainer>
    </Container>
  )
}
export { Product };