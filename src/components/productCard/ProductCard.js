import React from 'react';
import { Card, Popover, Space, Button } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { Container, ImageContainer, ProductImage, ButtonContainer } from './style';
import cd from '../img/cd.png';

class ProductCard extends React.Component {

    render() {

        const { name, image, style, price } = this.props.product;
        
        const content = (
            <div>
                <p>{style}</p>
                <p>{price}£</p>
            </div>
        );
        return (
            <Container>
                <Space>
                    <Popover placement="bottom" content={content} title="Description">
                        <Card style={{ width: 240 }} title={name}>
                            <ImageContainer>
                                <ProductImage src={image} />
                            </ImageContainer>
                            <ButtonContainer>
                            <Button type="primary" shape="round" icon={<ShoppingCartOutlined />} size='large' />
                            </ButtonContainer>
                            
                        </Card>
                    </Popover>
                </Space>
            </Container>
        )
    }
}
export default ProductCard;